package practice;

public class Main {

	public static void main(String[] args) {
		Persona persona1 = new Persona("Nicolas", 21);
		Persona ejemploPersona = new Persona("Pepito", 99);

		PersonaJSON personaJSON = new PersonaJSON();
		personaJSON.agregarPersona(persona1);
		personaJSON.agregarPersona(ejemploPersona);

		String jsonPretty = personaJSON.generarJSON();

		personaJSON.guardarJSON(jsonPretty, "Personas.JSON");

		
		System.out.println(personaJSON.leerJSON("Personas.JSON").getPersonas().toString());
	}

}
