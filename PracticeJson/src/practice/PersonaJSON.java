package practice;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.*;


public class PersonaJSON {

	private ArrayList<Persona> personas;
	
	public PersonaJSON() {
		this.personas=new ArrayList<Persona>();
	}
	
	
	
	public ArrayList<Persona> getPersonas() {
		return personas;
	}



	public void agregarPersona(Persona persona) {
		this.personas.add(persona);
	}
	
		
	public String generarJSON() {
		Gson gson= new GsonBuilder().setPrettyPrinting().create();
		String json= gson.toJson(this);
		
		return json;
	}
	
	public void guardarJSON(String jsonParaGuardar,String archivoDestino) {
		
		try {
			
			FileWriter writer=new FileWriter(archivoDestino);
			writer.write(jsonParaGuardar);
			writer.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}	
	}
	
	public static PersonaJSON leerJSON(String archivo) {
		Gson gson= new Gson();
		PersonaJSON ret=null;
		
		try {
			BufferedReader br= new BufferedReader(new FileReader(archivo));
			ret=gson.fromJson(br, PersonaJSON.class);
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
		

}
